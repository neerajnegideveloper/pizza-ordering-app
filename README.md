# Demo React.js 


#author : Neeraj negi
#Email  : Neerajnegi1003@gmail.com
---
This is a repository for MERN project
### Includes
- React
- Node
- Express
- Redux
### Run React
```
yarn install
yarn start
yarn build
```

### Run Server (Node)
```
yarn prod
```

```
- Application will run on http://localhost:3002
