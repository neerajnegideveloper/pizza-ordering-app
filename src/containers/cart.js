import React from "react";
import { connect } from "react-redux";

import { Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle, Modal, Button, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { bindActionCreators } from "redux";
import { clear_cart } from "../actions/product";
class Cart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };
  }

  componentDidMount() {
  }

  orderNow(values) {
    console.log("values", values);
    if (values) {
     /*  alert('You Have purchased '+ values.length + ' products');
      this.props.products.cart = [];
      this.props.history.push('/products'); */
      this.setState(prevState => ({
        modal: !prevState.modal
      }));
    }
  }


  modalClose() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }


  render() {
    const {cart} = this.props.products;
    return (
      <div>
        <h1>Product in your cart </h1>
        <div class="row">
          {cart.map((row,index) => (
            <div class = "col-md-4">
              <Card key={index}>
                <CardImg
                  top
                  height="200px"
                  width="200px"
                  src={row.image ? row.image.src : ""}
                  alt="Card image cap"
                />
                <CardBody>
                  <CardTitle>{row.product_name}</CardTitle>
                  <CardSubtitle>{row.product_price}</CardSubtitle>
                  <CardText>{row.product_description}</CardText>
                </CardBody>
              </Card>
            </div>
          ))}
        </div>
        <div>
          <Button onClick={() => this.orderNow(cart)}>Order Now</Button>
        </div>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>Order Receipt</ModalHeader>
          <ModalBody>
            Congratulation, You have successfully purchased the below product !!!
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={()=>this.modalClose()}>Ok</Button>{' '}
            {/* <Button color="secondary" onClick={this.toggle}>Cancel</Button> */}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  products: state.products
});

const mapDispatchToProps = dispatch => ({
  clearCart: bindActionCreators(clear_cart, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Cart);
