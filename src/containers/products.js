import React from "react";
import { connect } from "react-redux";
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button
} from "reactstrap";
import { bindActionCreators } from "redux";
import { getProductListing, add_to_cart } from "../actions/product";
class Products extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.getProductListing();
  }

  render() {
    console.log('this.props: ', this.props);
    const {items, cart} = this.props.products;
    return (
      <div>
        <h1>Products</h1>
        <div className="row">
          {items.map((row,index) => (
            <div className = "col-md-4">
              <Card key={index}>
                <CardImg
                  top
                  height="200px"
                  width="200px"
                  src={row.image ? row.image.src : ""}
                  alt="Card image cap"
                />
                <CardBody>
                  <CardTitle>{row.product_name}</CardTitle>
                  <CardSubtitle>{row.product_price}</CardSubtitle>
                  <CardText>{row.product_description}</CardText>
                  { 
                    cart.findIndex(item => item.id === row.id) > -1 ? 
                    <Button >Added</Button>
                    :
                    <Button onClick={() => this.props.addToCart(row)}>Add to cart</Button>
                  }
                
                </CardBody>
              </Card>
            </div>
          ))}
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  products: state.products
});

const mapDispatchToProps = dispatch => ({
  getProductListing: bindActionCreators(getProductListing, dispatch),
  addToCart: bindActionCreators(add_to_cart, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Products);
