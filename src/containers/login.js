import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { LocalForm, Control, Errors } from "react-redux-form";
import { Button, FormGroup, Label } from "reactstrap";
import { login } from '../actions/user';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleSubmitFailed = this.handleSubmitFailed.bind(this);
  }
  handleSubmit(values) {
      this.props.login(values, res => {
        if(res) {
          console.log('res ', res);
          this.setState({
            login: true
          });
          this.props.history.push('/products');
        } else {
          this.setState({
            login: false
          });
          alert('You are not authorised to login !!');
        }
    });
  }

  handleSubmitFailed(errors) {
    console.log("errors", errors);
  }

  render() {
    console.log("this.props", this.props);
    return (
      <div className="col-md-6 offset-3">
        <LocalForm
          onSubmit={values => this.handleSubmit(values)}
          onSubmitFailed={this.handleSubmitFailed}
          model="user"
        >
          <FormGroup>
            <Label for="email">Email</Label>
            <Control.text
              model=".email"
              type="email"
              className="form-control"
              validators={{ isRequired: val => val && val.length }}
            />
            <Errors
              model=".email"
              show="touched"
              messages={{
                isRequired: "Please provide an email address."
              }}
            />
          </FormGroup>
          <FormGroup>
            <Label for="password">Password</Label>
            <Control.text
              model=".password"
              type="password"
              className="form-control"
            />
            <Errors
              model=".email"
              show="touched"
              messages={{
                isRequired: "Please provide an email address."
              }}
            />
          </FormGroup>
          <FormGroup>
            <Button type="submit" className="btn btn-primary">
              Login
            </Button>
          </FormGroup>
        </LocalForm>
      </div>
    );
  }
}

// const mapStateToProps = state => ({
//   user: state.user
// });

const mapDispatchToProps = dispatch => ({
  login: bindActionCreators(login, dispatch)
});

export default connect(
  null,
  mapDispatchToProps
)(Login);
