/*
 * @file: messages.js
 * @description: Handle staic messages for the application
 * @author: 
 */

const Message = {
    emptyField: "Please Enter Value",
    success: "Success",
    error: "Error!",
    commonError: "Something went wrong!",
    logout: "Logout!"
  };
  
  module.exports = Message;
  