/*********** Routes for applications **************/
import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Cart from '../containers/cart';
import Login from '../containers/login';
import Products from '../containers/products';
import Header from '../components/header';
import NotFound from '../components/NoFound';

const Routers = () => {
    return (            
        <Router>
            <Header/>
            <Switch>
                <Route exact path="/" component={Login} />
                <Route exact path="/products" component={Products} />
                <Route exact path="/cart" component={Cart} />
                <Route path="*" component={NotFound} />
            </Switch>
        </Router>
    );
};

export default Routers;