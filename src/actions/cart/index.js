/*
 * @file: index.js
 * @description: It Contain Product Account Related Action Creators.
 * @author: 
 */

import * as TYPE from '../constants';

export const get_cart_list = (data) => ({ type: TYPE.GET_CART_LIST, data });
export const order_now = (data) => ({ type: TYPE.ORDER_NOW, data });