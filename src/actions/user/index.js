/*
 * @file: index.js
 * @description: It Contain Product Account Related Action Creators.
 * @author: 
 */

import * as TYPE from '../constants';
import ApiClient from '../../api-client';

export const login_success = (data) => ({ type: TYPE.LOGIN_SUCCESS, data });
// Thunk Action Creators For Api
/****** action creator for login ********/
export const login = (params, callback) => {
    
    return dispatch => {
        /* let reqData = {
            "email" : "jatinderkumar0550@gmail.com",
            "password"  : "admin@123"
        } */
        ApiClient.post('http://localhost/apis/api/auth/login', params).then(result => {
            if (result.success) {
                console.log('result : ', result);
               dispatch(login_success(result));
               callback(true);
            }
            else {
                callback(false);
            }
        });
    };
};