import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from "react-router-dom";
import { Navbar } from 'reactstrap';

export default () => {
    const {user} = useSelector(state => state);
    console.log('state>>>>>>>>>>>>>', user);
    return (
      <div>
         <Navbar color="faded" light>
       
         {
          user.isLogin ? 
          <React.Fragment>
            <Link to="/products" className="nav-link">Products</Link>
            <Link to="/cart" className="nav-link">Cart</Link>
          </React.Fragment>
          :
          <Link to="/" className="mr-auto navbar-brand">Pizza Delivery App</Link>
        }
        </Navbar>
        
      </div>
    );
  };