/*
 * @file: product.js
 * @description: Reducers and actions for store/manipulate products's  data
 * @author: 
*/

import * as TYPE from '../../actions/constants';


/******** Reducers ********/
const initialState = {
  items: [],
  cart:[],
  order: []
};
export default function reducer(state = initialState, action) {
  switch (action.type) {
    // case TYPE.GET_PRODUCT_LIST:
    //   return  {...state, items: action.data};
    case TYPE.ORDER_NOW:
      return  {...state, order: [...state.order, action.data]};
    default:
      return state;
  }
}