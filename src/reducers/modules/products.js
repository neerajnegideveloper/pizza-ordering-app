/*
 * @file: product.js
 * @description: Reducers and actions for store/manipulate products's  data
 * @author: 
*/

import * as TYPE from '../../actions/constants';


/******** Reducers ********/
const initialState = {
  items: [],
  cart:[]
};
export default function reducer(state = initialState, action) {
  switch (action.type) {
    case TYPE.GET_PRODUCT_LIST:
      return  {...state, items: action.data};
    case TYPE.ADD_TO_CART:
      return  {...state, cart: [...state.cart, action.data]};
    case TYPE.CLEAR_CART:
      return  {...state, cart: []};
    default:
      return state;
  }
}